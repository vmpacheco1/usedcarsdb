# UsedCarDB Project
This repository contains the code I wrote for the 'frontend' of my Database Design & Applications final project in Fall 2022.

The program runs through a command line interface (called via script wrote in Batch as I was working in a Windows environment at the time). It prompts the user for the criteria they would like to query the database for, then calls a stored procedure on the database side to print out the results of the query in an table-like format.

I refactored the code for this project for an assignment for a Software Process Management course when we were going over Clean Code principles in Fall 2023.