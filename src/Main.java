// Import MySQL library, scanner
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;
import java.sql.ResultSet;

public class Main {
    public static void main (String [] args) {
        // Create scanner, variable to keep application running
        Scanner sc = new Scanner(System.in);
        boolean run = true;

        // While loop keeps application running while user wants to prompt
        while (run) {
            // Prompt user for query information
            System.out.println("\nUsed Car Search\n(Leave fields empty to broaden search)");

            boolean hasAccidents = promptAccidents(sc);
            boolean hadOneOwner = promptOwners(sc);
            int maxCost = promptCost(sc);
            int maxMileage = promptMileage(sc);
            double minDealerRating = promptDealerRating(sc);
            // If empty will be ignored, otherwise brand will be specified in query
            System.out.print("Brand: ");
            String brand = sc.nextLine();

            System.out.println();

            // Print out query
            ResultSet results = carQuery(hasAccidents, hadOneOwner, brand, maxCost, maxMileage, minDealerRating);
            printResults(results);

            // Ask if user would like to create a new query, set run boolean to false and end loop if user inputs "no"
            System.out.print("\nWould you like to search again (y/n)? ");
            run = sc.nextLine().equalsIgnoreCase("y");

            // Create a divider if querying again
            if (run) {
                System.out.println("\n---------------------------------------------------------------------------------\n");
            }
        }

        // Close scanner
        sc.close();
    }

    public static boolean promptAccidents(Scanner sc) {
            // Yes excludes cars with accidents in query, any other input is ignored
            System.out.print("\nExclude vehicles with Accidents? (y/n): ");
            String accidentsStr = sc.nextLine();
            return accidentsStr.equalsIgnoreCase("y");
    }

    public static boolean promptOwners(Scanner sc) {
            // Yes excludes cars with more than one owner, any other input is ignored
            System.out.print("Exclude vehicles with more than one owner?(y/n): ");
            String ownersStr = sc.nextLine();
            return ownersStr.equalsIgnoreCase("y");
    }

    public static int promptCost(Scanner sc) {
            // Sets upper bound of cost for query, if empty set to -1
            System.out.print("Maximum Price: $");
            String costStr = sc.nextLine();
            if (costStr.length() > 0) {
                return (int)Double.parseDouble(costStr);
            } else {
                return -1;
            }
    }

    public static int promptMileage(Scanner sc) {
            // Sets upper bound of mileage for query, if empty set to -1
            System.out.print("Maximum Mileage: ");
            String mileStr = sc.nextLine();
            if (mileStr.length() > 0) {
                return Integer.parseInt(mileStr);
            } else {
                return -1;
            }
    }

    public static double promptDealerRating(Scanner sc) {
            // Set minimum dealership rating, cannot surpass 5.0, if empty set to -1
            System.out.print("Minimum Dealer Rating (ex. 1.0/5.0): ");
            String rateStr = sc.nextLine();
            double rating;
            if (rateStr.length() > 0) {
                rating = Double.parseDouble(rateStr);
                if (rating > 5.0) {
                    return 5.0;
                } else {
                    return rating;
                }
            } else {
                return -1.0;
            }
    }

    // carQuery - performs the query and prints out the desired information
    public static ResultSet carQuery(boolean accidents, boolean owners, String brand, int cost, int mileage, double rating) {

        // Set up query, result set
        String query = "{ call used_car_query(?, ?, ?, ?, ?, ?) }";
        ResultSet results;

        try(Connection conn = UsedCarConnector.getConnection(); // Create the connection
        CallableStatement stmt = conn.prepareCall(query)) { // Set up the SQL statement

            // Set parameters for SQL stored procedure
            stmt.setBoolean(1, accidents);
            stmt.setBoolean(2, owners);
            stmt.setString(3, brand);
            stmt.setInt(4, cost);
            stmt.setInt(5, mileage);
            stmt.setDouble(6, rating);

            // Execute query
            results = stmt.executeQuery();
            
            return results;

        // Catch any input error
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return null;
    }

    public static void printResults(ResultSet results) {

            int numResults = 0;
            // Print out results
            System.out.printf("%-20s%-9s%-16s%-32s%-7s%-10s%-9s%-12s%-29s%-16s%-50s%-15s |\n\n",
            "| VIN", "| Price", "| Brand", "| Model", "| Year", "| Mileage", "| Owners", "| Accidents", "| Dealership",
            "| Dealer Rating", "| Address", "| Phone #");
            try {
                while(results.next()) {
                    System.out.printf("%-20s%-9s%-16s%-32s%-7s%-10s%-9s%-12s%-29s%-16s%-50s%-15s |\n",
                            "| " + results.getString("vin"),
                            "| " + results.getString("price"),
                            "| " + results.getString("brand"),
                            "| " + results.getString("model"),
                            "| " + results.getInt("year"),
                            "| " + results.getInt("mileage"),
                            "| " + results.getInt("owners"),
                            "| " + results.getInt("accidents"),
                            "| " + results.getString("name"),
                            "| " + results.getDouble("rating") + " / 5.0",
                            "| " + results.getString("address"),
                            "| " + results.getString("phone"));
                    numResults++;
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
            // Print out number of results
            System.out.println("\n" + numResults + " matching results found.");
    }

}