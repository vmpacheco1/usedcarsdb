import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

// MySQL Java Connector - Credit to Nada Alsallami
public class UsedCarConnector {

    public static Connection getConnection() throws SQLException {
        // If the following connection fails, this leaves the program without a connection
        Connection connection = null;

        try {

            String url = "jdbc:mysql://127.0.0.1:3307/usedcars";
            String user = "guest";
            connection = DriverManager.getConnection(url, user, "guest");

        } catch(SQLException exception) {
            System.out.println(exception.getMessage());
        }

        return connection;
    }
}
